export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case 'increment':
      return { count: state.count + 1 }
    case 'decrement':
      return { count: state.count - 1 }
    case 'GET_PETS':
      return {
        ...state,
        pets: payload,
      }
    case 'CHANGE_NAME':
      return {
        ...state,
        dataFromApi: {
          ...state.dataFromApi,
          name: payload,
        },
      }
    case 'CHANGE_COLLAPSE_TITLE':
      return {
        ...state,
        collapses: state.collapses.map((col, index) => {
          const { uuid } = col
          if (uuid === payload.uuid) {
            return {
              ...col,
              title: payload.title,
            }
          }
          return col
        }),
      }
    default:
      throw new Error()
  }
}
