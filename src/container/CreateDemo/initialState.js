import { v4 as uuidv4 } from "uuid";

export default {
  team: "Pho",
  dataFromApi: {
    name: "tim",
    company: "aleph"
  },
  pets: [],
  collapses: [
    {
      uuid: uuidv4(),
      isVisible: true,
      title: "collapse 0",
      cards: [
        {
          uuid: uuidv4(),
          isVisible: false,
          formGroups: [
            {
              uuid: uuidv4(),
              isVisible: false,
              label: {
                title: "form group 1",
                subtitle: "small description for form group 1"
              },
              entry: {
                // from here, props depend on existing composite component cib-ui-component
                componentName: "NOTES",
                component: null,
                fields: [
                  {
                    uuid: uuidv4(),
                    isVisible: true,
                    componentName: "TEXT_INPUT",
                    component: null,
                    type: "text", // map to atom or composite component
                    value: "default"
                  },
                  {
                    uuid: uuidv4(),
                    isVisible: true,
                    componentName: "CHECKBOX",
                    component: null,
                    type: "checkbox", // map to atom or composite component
                    value: "default"
                  }
                ]
              }
            }
          ]
        }
      ]
    }
  ]
};
