import React, { useReducer, useEffect } from 'react'
import { Layout, Button, Space } from 'antd'
import { PaymentContext } from './context'
import reducer from './reducer'
import { getPets } from './action'
import initialPaymentState from './initialState'
import Collapse from 'component/Collapse'

const { Header, Content } = Layout

const CreateDemo = () => {
  const [paymentState, dispatchPaymentState] = useReducer(
    reducer,
    initialPaymentState
  )

  useEffect(() => {
    getPets((response) => {
      dispatchPaymentState({ type: 'GET_PETS', payload: response })
    })
  }, [])

  const handleChangeCollapeTitle = ({ uuid, title }) => {
    dispatchPaymentState({
      type: 'CHANGE_COLLAPSE_TITLE',
      payload: {
        uuid,
        title,
      },
    })
  }

  return (
    <PaymentContext.Provider value={paymentState}>
      <Layout>
        <Header>
          Welcome {paymentState.dataFromApi.name} @{' '}
          {paymentState.dataFromApi.company} and I have{' '}
          {paymentState.pets.length} pets;
        </Header>
        <Content>
          <Space direction="vertical" style={{ width: '100%' }}>
            <Collapse
              collapses={paymentState.collapses}
              handleChangeCollapeTitle={handleChangeCollapeTitle}
            ></Collapse>
            <Button
              type="primary"
              onClick={() =>
                dispatchPaymentState({
                  type: 'CHANGE_NAME',
                  payload: 'FaiZal',
                })
              }
            >
              i can change the name
            </Button>
          </Space>
        </Content>
      </Layout>
    </PaymentContext.Provider>
  )
}

export default CreateDemo
