import React, { useState, useEffect } from 'react'
import '@elastic/eui/dist/eui_theme_dark.css'
// import '@elastic/eui/dist/eui_theme_light.css'
import {
  EuiPage,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiPageHeader,
  EuiPageHeaderSection,
  EuiPageSideBar,
  EuiTitle,
} from '@elastic/eui'
import SideNav from './SideNav'

const InitialContent = () => {
  return (
    <>
      <EuiPageContentHeader>
        <EuiPageContentHeaderSection>
          <EuiTitle>
            <h2>APIs grouped by modules</h2>
          </EuiTitle>
        </EuiPageContentHeaderSection>
        {/* <EuiPageContentHeaderSection>
              Content abilities
            </EuiPageContentHeaderSection> */}
      </EuiPageContentHeader>
      <EuiPageContentBody>
        API documentation based on module flows of S2B
      </EuiPageContentBody>
    </>
  )
}

export default () => {
  const [currentPath, setPath] = useState('')
  const [LoadableComponent, setLoadableComponent] = useState('div')
  const loadComponent = (path) => {
    console.log('path', path)
    setPath(path)
  }

  useEffect(() => {
    if (currentPath) {
      import(`./${currentPath}`).then((com) => {
        setLoadableComponent(() => com.default)
      })
    }
  }, [currentPath])

  // const LoadableComponent = Loadable({
  //   loader: () => import(`./${currentPath}`),
  //   loading() {
  //     return <EuiLoadingContent lines={8} />
  //   },
  // })

  return (
    <EuiPage>
      <EuiPageSideBar>
        <SideNav loadComponent={loadComponent}></SideNav>
      </EuiPageSideBar>
      <EuiPageBody component="div">
        <EuiPageHeader>
          <EuiPageHeaderSection>
            <EuiTitle size="l">
              <h1>S2B API Documentation</h1>
            </EuiTitle>
          </EuiPageHeaderSection>
          {/* <EuiPageHeaderSection>Page abilities</EuiPageHeaderSection> */}
        </EuiPageHeader>
        <EuiPageContent>
          {!currentPath ? <InitialContent /> : <LoadableComponent />}
        </EuiPageContent>
      </EuiPageBody>
    </EuiPage>
  )
}
