import React from 'react'

import { EuiIcon, EuiSideNav } from '@elastic/eui'

export default ({ loadComponent }) => {
  const toggleOpenOnMobile = () => {
    // setIsSideNavOpenOnMobile(!isSideNavOpenOnMobile)
  }

  const selectItem = (path) => {
    // setSelectedItem(path)
    loadComponent && loadComponent(path)
  }

  const createItem = (name, data = {}) => {
    // NOTE: Duplicate `name` values will cause `id` collisions.
    return {
      forceOpen: true,
      ...data,
      id: name,
      name,
      onClick: () => selectItem(data.path),
    }
  }

  const sideNav = [
    createItem('ID Registration', {
      icon: <EuiIcon type="gear" />,
      items: [
        createItem('Folder Structure', {
          path: 'IDReg/FolderTree.jsx',
        }),
      ],
    }),
    createItem('Tax & Discounts', {
      icon: <EuiIcon type="gear" />,
      items: [
        createItem('Folder Structure', {
          path: 'TaxDiscounts/FolderTree.jsx',
        }),
        createItem('Create', {
          items: [
            createItem('getApplicableType', {
              path: 'TaxDiscounts/create/getApplicableType.jsx',
            }),
            createItem('getCountryList', {
              path: 'TaxDiscounts/create/getCountryList.jsx',
            }),
            createItem('saveTaxandDiscount', {
              path: 'TaxDiscounts/create/saveTaxandDiscount.jsx',
            }),
          ],
        }),
        createItem('Manage', {
          items: [
            createItem('qlService-getListViewProducts', {
              path: 'TaxDiscounts/manage/qlService-getListViewProducts.jsx',
            }),
            createItem('qlService-getListHeaderDetails', {
              path: 'TaxDiscounts/manage/qlService-getListHeaderDetails.jsx',
            }),
            createItem('qlService-getFilterData', {
              path: 'TaxDiscounts/manage/qlService-getFilterData.jsx',
            }),
            createItem('qlService-getMultiColumnSort', {
              path: 'TaxDiscounts/manage/qlService-getMultiColumnSort.jsx',
            }),
            createItem('deleteTaxAndDisc', {
              path: 'TaxDiscounts/manage/deleteTaxAndDisc.jsx',
            }),
            createItem('printTaxAndDisc', {
              path: 'TaxDiscounts/manage/printTaxAndDisc.jsx',
            }),
          ],
        }),
      ],
    }),
  ]

  return (
    <EuiSideNav
      mobileTitle="Navigate within $APP_NAME"
      toggleOpenOnMobile={toggleOpenOnMobile}
      isOpenOnMobile={true}
      items={sideNav}
      style={{ width: 192 }}
    />
  )
}
