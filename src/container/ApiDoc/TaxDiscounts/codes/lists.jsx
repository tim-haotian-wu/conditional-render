import React from 'react'
import { EuiCodeBlock } from '@elastic/eui'
export default () => {
  return (
    <EuiCodeBlock
      language="js"
      fontSize="m"
      paddingSize="m"
      overflowHeight={300}
      isCopyable
    >{`const code = 'lists'`}</EuiCodeBlock>
  )
}
