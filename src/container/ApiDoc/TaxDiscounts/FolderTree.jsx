import React, { useState, useEffect } from 'react'

import {
  EuiIcon,
  EuiTreeView,
  EuiToken,
  EuiSpacer,
  // EuiLoadingContent,
} from '@elastic/eui'

const FolderTree = () => {
  const [codeId, setCodeId] = useState('')
  const [CodeBlock, setCodeBlock] = useState('div')
  const showAlert = () => {
    alert('You squashed a bug!')
  }

  useEffect(() => {
    if (codeId) {
      import(`./codes/${codeId}.jsx`).then((com) => {
        setCodeBlock(() => com.default)
      })
    }
  }, [codeId])

  //   const LoadableCode = Loadable({
  //     loader: () => import(`./codes/${codeId}.jsx`),
  //     loading() {
  //       return <EuiLoadingContent lines={5} />
  //     },
  //   })

  const items = [
    {
      label: 'lists',
      id: 'lists',
      icon: <EuiIcon type="folderClosed" />,
      iconWhenExpanded: <EuiIcon type="folderOpen" />,
      isExpanded: true,
      children: [
        {
          label: 'Item A',
          id: 'item_a',
          icon: <EuiIcon type="document" />,
          callback: () => setCodeId('lists'),
        },
        {
          label: 'Item B',
          id: 'item_b',
          icon: <EuiIcon type="arrowRight" />,
          iconWhenExpanded: <EuiIcon type="arrowDown" />,
          children: [
            {
              label: 'A Cloud',
              id: 'item_cloud',
              icon: <EuiToken iconType="tokenConstant" />,
            },
            {
              label: "I'm a Bug",
              id: 'item_bug',
              icon: <EuiToken iconType="tokenEnum" />,
              callback: showAlert,
            },
          ],
        },
        {
          label: 'Item C',
          id: 'item_c',
          icon: <EuiIcon type="arrowRight" />,
          iconWhenExpanded: <EuiIcon type="arrowDown" />,
          children: [
            {
              label: 'Another Cloud',
              id: 'item_cloud2',
              icon: <EuiToken iconType="tokenConstant" />,
            },
            {
              label:
                'This one is a really long string that we will check truncates correctly',
              id: 'item_bug2',
              icon: <EuiToken iconType="tokenEnum" />,
              callback: showAlert,
            },
          ],
        },
      ],
    },
    {
      label: 'Item Two',
      id: 'item_two',
    },
  ]

  return (
    <div style={{ width: '100%' }}>
      <EuiTreeView items={items} aria-label="Sample Folder Tree" />
      <EuiSpacer />
      {codeId ? <CodeBlock /> : null}
    </div>
  )
}

export default FolderTree
