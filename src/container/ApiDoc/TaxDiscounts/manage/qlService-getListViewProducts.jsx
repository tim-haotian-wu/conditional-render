// getListViewProducts
import React from 'react'
// import '@elastic/eui/dist/eui_theme_light.css'

import {
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
  EuiCodeBlock,
  EuiSpacer,
} from '@elastic/eui'

export default () => (
  <>
    <EuiPageContentHeader>
      <EuiPageContentHeaderSection>
        <EuiTitle>
          <h2>Manage - getListViewProducts</h2>
        </EuiTitle>
      </EuiPageContentHeaderSection>
      {/* <EuiPageContentHeaderSection>
            Content abilities
          </EuiPageContentHeaderSection> */}
    </EuiPageContentHeader>
    <EuiPageContentBody>
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// End Point
/s2b/global/corporate/api/payment/qlServices
`}
      </EuiCodeBlock>

      <EuiSpacer />
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// Sample Request
{
  "operationName": "getListViewProducts",
  "variables": { "getApplicableProductRqst": { "userRole": "O" } },
  "query": "query getListViewProducts($getApplicableProductRqst: GetApplicableProductRqst) {\n  getListViewProducts(getApplicableProductRqst: $getApplicableProductRqst) {\n    transactionSuccess\n    roleBasedProductDetails {\n      userRole\n      productList\n      subProductList {\n        id\n        label\n        displayIndex\n        product\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"
}
`}
      </EuiCodeBlock>
    </EuiPageContentBody>
  </>
)
