import React from 'react'
// import '@elastic/eui/dist/eui_theme_light.css'

import {
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
  EuiCodeBlock,
  EuiSpacer,
} from '@elastic/eui'

export default () => (
  <>
    <EuiPageContentHeader>
      <EuiPageContentHeaderSection>
        <EuiTitle>
          <h2>Manage - deleteTaxAndDisc</h2>
        </EuiTitle>
      </EuiPageContentHeaderSection>
      {/* <EuiPageContentHeaderSection>
            Content abilities
          </EuiPageContentHeaderSection> */}
    </EuiPageContentHeader>
    <EuiPageContentBody>
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// End Point
/s2b/global/corporate/api/payment/taxAndDiscount/managelist/deleteTaxAndDisc
        `}
      </EuiCodeBlock>

      <EuiSpacer />
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// Sample Request
const request = {
  taxAndDiscList: [
    { ctryCode: 'CN', versionNo: '1', taxType: 'DISC', recordType: 'iu' },
  ],
}
        `}
      </EuiCodeBlock>
    </EuiPageContentBody>
  </>
)
