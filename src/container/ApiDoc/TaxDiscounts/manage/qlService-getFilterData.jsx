// getListViewProducts
import React from 'react'
// import '@elastic/eui/dist/eui_theme_light.css'

import {
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
  EuiCodeBlock,
  EuiSpacer,
} from '@elastic/eui'

export default () => (
  <>
    <EuiPageContentHeader>
      <EuiPageContentHeaderSection>
        <EuiTitle>
          <h2>Manage - getFilterData</h2>
        </EuiTitle>
      </EuiPageContentHeaderSection>
      {/* <EuiPageContentHeaderSection>
            Content abilities
          </EuiPageContentHeaderSection> */}
    </EuiPageContentHeader>
    <EuiPageContentBody>
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// End Point
/s2b/global/corporate/api/payment/qlServices
`}
      </EuiCodeBlock>

      <EuiSpacer />
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// Sample Request
{
  "operationName": "getFilterData",
  "variables": {
    "getFilterDataRqst": { "operation": "MANAGE", "entity": "TAXDISC" }
  },
  "query": "query getFilterData($getFilterDataRqst: GetFilterDataRqst) {\n  getFilterData(getFilterDataRqst: $getFilterDataRqst) {\n    filterList {\n      filterID\n      filterLabelName\n      filterType\n      filterOrder\n      listData {\n        itemId\n        label\n        value\n        minAmountRange\n        maxAmountRange\n        __typename\n      }\n      __typename\n    }\n    paginationSizeList {\n      label\n      value\n      __typename\n    }\n    __typename\n  }\n}\n"
}
`}
      </EuiCodeBlock>
    </EuiPageContentBody>
  </>
)
