// getListViewProducts
import React from 'react'
// import '@elastic/eui/dist/eui_theme_light.css'

import {
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
  EuiCodeBlock,
  EuiSpacer,
} from '@elastic/eui'

export default () => (
  <>
    <EuiPageContentHeader>
      <EuiPageContentHeaderSection>
        <EuiTitle>
          <h2>Manage - getMultiColumnSort</h2>
        </EuiTitle>
      </EuiPageContentHeaderSection>
      {/* <EuiPageContentHeaderSection>
            Content abilities
          </EuiPageContentHeaderSection> */}
    </EuiPageContentHeader>
    <EuiPageContentBody>
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// End Point
/s2b/global/corporate/api/payment/qlServices
`}
      </EuiCodeBlock>

      <EuiSpacer />
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// Sample Request
{
  "operationName": "getMultiColumnSort",
  "variables": {
    "getMultiColumnSortRqst": { "operation": "MANAGE", "entity": "TAXDISC" }
  },
  "query": "query getMultiColumnSort($getMultiColumnSortRqst: GetMultiColumnSortRqst) {\n  getMultiColumnSort(getMultiColumnSortRqst: $getMultiColumnSortRqst) {\n    transactionSuccess\n    prioritySort {\n      id\n      label\n      sortKey\n      isSortable\n      product\n      sortorder\n      __typename\n    }\n    __typename\n  }\n}\n"
}
`}
      </EuiCodeBlock>
    </EuiPageContentBody>
  </>
)
