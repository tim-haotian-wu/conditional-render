// getListViewProducts
import React from 'react'
// import '@elastic/eui/dist/eui_theme_light.css'

import {
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
  EuiCodeBlock,
  EuiSpacer,
} from '@elastic/eui'

export default () => (
  <>
    <EuiPageContentHeader>
      <EuiPageContentHeaderSection>
        <EuiTitle>
          <h2>Manage - getListHeaderDetails</h2>
        </EuiTitle>
      </EuiPageContentHeaderSection>
      {/* <EuiPageContentHeaderSection>
            Content abilities
          </EuiPageContentHeaderSection> */}
    </EuiPageContentHeader>
    <EuiPageContentBody>
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// End Point
/s2b/global/corporate/api/payment/qlServices
`}
      </EuiCodeBlock>

      <EuiSpacer />
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// Sample Request
{
  "operationName": "getListHeaderDetails",
  "variables": {
    "getListHdrDtlsRqst": {
      "operation": "MANAGE",
      "entity": "TAXDISC",
      "groupingCriteria": null,
      "isPageLoader": true
    }
  },
  "query": "query getListHeaderDetails($getListHdrDtlsRqst: GetListHdrDtlsRqst) {\n  getListHeaderDetails(getListHdrDtlsRqst: $getListHdrDtlsRqst) {\n    globalActionList {\n      itemId\n      label\n      __typename\n    }\n    refData {\n      itemId\n      list\n      __typename\n    }\n    listData {\n      columns\n      rows\n      __typename\n    }\n    attributeList {\n      attributeId\n      labelName\n      colDataType\n      displayIndex\n      isSortable\n      highlight\n      clickable\n      colWidth\n      columnAlign\n      __typename\n    }\n    lasyLoadingSize\n    paginationSize\n    groupingCriteria {\n      attributeName\n      attributeValues\n      __typename\n    }\n    __typename\n  }\n}\n"
}

`}
      </EuiCodeBlock>
      <EuiSpacer />
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// filter - sorting
{
  "operationName": "getListHeaderDetails",
  "variables": {
    "getListHdrDtlsRqst": {
      "operation": "MANAGE",
      "entity": "TAXDISC",
      "sortCriteria": [],
      "groupingCriteria": [],
      "isPageLoader": false,
      "paginationSize": "20^20"
    }
  },
  "query": "query getListHeaderDetails($getListHdrDtlsRqst: GetListHdrDtlsRqst) {\n  getListHeaderDetails(getListHdrDtlsRqst: $getListHdrDtlsRqst) {\n    globalActionList {\n      itemId\n      label\n      __typename\n    }\n    refData {\n      itemId\n      list\n      __typename\n    }\n    listData {\n      columns\n      rows\n      __typename\n    }\n    attributeList {\n      attributeId\n      labelName\n      colDataType\n      displayIndex\n      isSortable\n      highlight\n      clickable\n      colWidth\n      columnAlign\n      __typename\n    }\n    lasyLoadingSize\n    paginationSize\n    groupingCriteria {\n      attributeName\n      attributeValues\n      __typename\n    }\n    __typename\n  }\n}\n"
}
`}
      </EuiCodeBlock>
    </EuiPageContentBody>
  </>
)
