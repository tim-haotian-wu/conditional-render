import React from 'react'
// import '@elastic/eui/dist/eui_theme_light.css'

import {
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
  EuiCodeBlock,
  EuiSpacer,
} from '@elastic/eui'

export default () => (
  <React.Fragment>
    <EuiPageContentHeader>
      <EuiPageContentHeaderSection>
        <EuiTitle>
          <h2>Create - saveTaxandDiscount</h2>
        </EuiTitle>
      </EuiPageContentHeaderSection>
      {/* <EuiPageContentHeaderSection>
            Content abilities
          </EuiPageContentHeaderSection> */}
    </EuiPageContentHeader>
    <EuiPageContentBody>
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// End Point
/s2b/global/corporate/api/payment/taxAndDiscount/create/saveTaxandDiscount
        `}
      </EuiCodeBlock>

      <EuiSpacer />
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// Sample Request
const request = {
  paymentTransaction: {
    versionNo: 1,
    debitAccount: { countryCode: "CN", cityCode: "SHA" },
    paymentTaxDiscountDetailList: [
      {
        taxType: "DISC",
        recordType: "iu",
        description: "ijiu",
        percent: "6",
        taxdiscrefno: ""
      }
    ]
  }
};
        `}
      </EuiCodeBlock>
    </EuiPageContentBody>
  </React.Fragment>
)
