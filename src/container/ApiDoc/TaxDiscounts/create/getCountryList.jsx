import React from 'react'
// import '@elastic/eui/dist/eui_theme_light.css'

import {
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
  EuiCodeBlock,
  EuiSpacer,
} from '@elastic/eui'

export default () => (
  <React.Fragment>
    <EuiPageContentHeader>
      <EuiPageContentHeaderSection>
        <EuiTitle>
          <h2>Create - getCountryList</h2>
        </EuiTitle>
      </EuiPageContentHeaderSection>
      {/* <EuiPageContentHeaderSection>
            Content abilities
          </EuiPageContentHeaderSection> */}
    </EuiPageContentHeader>
    <EuiPageContentBody>
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// End Point
/s2b/global/corporate/api/payment/taxAndDiscount/create/getCountryList
        `}
      </EuiCodeBlock>

      <EuiSpacer />
      <EuiCodeBlock
        language="js"
        fontSize="m"
        paddingSize="m"
        overflowHeight={300}
        isCopyable
      >
        {`
// Sample Request
const request = {
};
        `}
      </EuiCodeBlock>
    </EuiPageContentBody>
  </React.Fragment>
)
