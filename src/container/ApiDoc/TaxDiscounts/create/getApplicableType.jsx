import React from 'react'
// import '@elastic/eui/dist/eui_theme_light.css'

import {
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
  EuiCodeBlock,
  EuiSpacer,
} from '@elastic/eui'

export default () => {
  //   const [code, setCode] = useState('')

  return (
    <React.Fragment>
      <EuiPageContentHeader>
        <EuiPageContentHeaderSection>
          <EuiTitle>
            <h2>Create - getApplicableType</h2>
          </EuiTitle>
        </EuiPageContentHeaderSection>
        {/* <EuiPageContentHeaderSection>
            Content abilities
          </EuiPageContentHeaderSection> */}
      </EuiPageContentHeader>
      <EuiPageContentBody>
        <EuiCodeBlock
          language="js"
          fontSize="m"
          paddingSize="m"
          overflowHeight={300}
          isCopyable
        >
          {`
// End Point
/s2b/global/corporate/api/payment/taxAndDiscount/create/getApplicableType
        `}
        </EuiCodeBlock>

        <EuiSpacer />
        <EuiCodeBlock
          language="js"
          fontSize="m"
          paddingSize="m"
          overflowHeight={300}
          isCopyable
        >
          {`
// Sample Request
const request = {
    paymentTransaction: { debitAccount: { countryCode: "UG", cityCode: "KLA" } }
};
        `}
        </EuiCodeBlock>
      </EuiPageContentBody>
    </React.Fragment>
  )
}
