import React, { useState, useEffect } from 'react'

import {
  EuiIcon,
  EuiTreeView,
  // EuiToken,
  EuiSpacer,
  // EuiLoadingContent,
} from '@elastic/eui'

const FolderTree = () => {
  const [codeId, setCodeId] = useState('')
  const [CodeBlock, setCodeBlock] = useState('div')

  useEffect(() => {
    if (codeId) {
      import(`./codes/${codeId}.jsx`).then((com) => {
        setCodeBlock(() => com.default)
      })
    }
  }, [codeId])

  //   const LoadableCode = Loadable({
  //     loader: () => import(`./codes/${codeId}.jsx`),
  //     loading() {
  //       return <EuiLoadingContent lines={5} />
  //     },
  //   })

  const items = [
    {
      label: 'CreateIDReg',
      id: 'CreateIDReg',
      icon: <EuiIcon type="folderClosed" />,
      iconWhenExpanded: <EuiIcon type="folderOpen" />,
      isExpanded: true,
      children: [
        {
          label: '__tests__',
          id: 'item_a',
          icon: <EuiIcon type="folderClosed" />,
          iconWhenExpanded: <EuiIcon type="folderOpen" />,
          children: [
            {
              label: 'index.test.js',
              id: 'index.test.js',
              icon: <EuiIcon type="wrench" />,
              callback: () => setCodeId('unit-test'),
            },
          ],
        },
      ],
    },
  ]

  return (
    <div style={{ width: '100%' }}>
      <EuiTreeView items={items} aria-label="Sample Folder Tree" />
      <EuiSpacer />
      {codeId ? <CodeBlock /> : null}
    </div>
  )
}

export default FolderTree
