import React from 'react'
import { EuiCodeBlock } from '@elastic/eui'
export default () => {
  return (
    <EuiCodeBlock
      language="js"
      fontSize="m"
      paddingSize="m"
      overflowHeight={300}
      isCopyable
    >{`
import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import CreateIDReg from "../index";

describe("Create ID Registration Flow", () => {
  const defaultProps = {
    something: "something"
  };
  // snapshot
  test("feature should render same snapshot everytime when apply default props (happy flow)", () => {
    render(<CreateIDReg {...defaultProps} />);
    expect(screen).toMatchSnapshot();
  });

  // static test when first time entry
  test("user should see common header bar when enter the page", () => {
    render(<CreateIDReg {...defaultProps} />);
    expect(screen.getByRole("header")).toBeInTheDocument();
    // expect(getByTestId("header")).toBeInTheDocument();
  });

  test("user should see information snapshot section in the header when enter the page", () => {
    render(<CreateIDReg {...defaultProps} />);
    expect(screen.getByTestId("snapshot")).toBeInTheDocument();
  });

  test("user should see a form with a title of ID REGISTRATION when enter the page", () => {
    render(<CreateIDReg {...defaultProps} />);
    expect(screen.getByTestId("create-id-registration")).toBeInTheDocument();
    expect(screen.getByText("ID REGISTRATION")).toBeInTheDocument();
  });

  test("user should see a dropdown for them to select ID Type in ID REGISTRATION form", () => {
    render(<CreateIDReg {...defaultProps} />);
    expect(screen.getAllByRole("combobox")[0]).toBeInTheDocument();
  });

  test("user should see a footer bar with cancel and submit buttons where they can go to previous page or submit the ID creation", () => {
    render(<CreateIDReg {...defaultProps} />);
    expect(screen.getByTestId("footer")).toBeInTheDocument();
    expect(screen.getByRole("button", { name: "cancel" })).toBeInTheDocument();
    expect(screen.getByRole("button", { name: "submit" })).toBeInTheDocument();
  });
  // form interactions
  test("user should see account number input after choose any one of the ID Type", () => {
    render(<CreateIDReg {...defaultProps} />);
    const InputIdType = screen.getAllByRole("combobox")[0];
    expect(InputIdType).toBeInTheDocument();
    userEvent.selectOptions(InputIdType, ["HK-FPSID"]); // action
    expect(screen.getByTestId("test-form")).toHaveValue("HK-FPSID");
    expect(screen.getByTestId("input-account-number")).toBeInTheDocument();
  });
  // HK-FPSID
  // invalid / not found account number
  test("user should see infomation toast at top of screen after search for invalid or not found account number when they have chosen HK-FPSID as ID Type", () => {});
  // valid account number
  test("user should see customer's name/display name after search for valid account number when they have chosen HK-FPSID as ID Type", () => {});
  // proxy id type
  test("use should see proxy id type after calling getProxyAccountEnquireService api", () => {});
  // meta snapshot
  test("use should see account / proxy value / id typ in ListHeader after calling getProxyAccountEnquireService api", () => {});

  // MMID
  // invalid / not found account number
  test("user should see infomation toast at top of screen after search for invalid or not found account number when they have chosen MMID as ID Type", () => {});
  // valid account number
  test("user should be able to enter their mobile number after search for valid account number when they have chosen MMID as ID Type", () => {});
  // country market code
  test("user should be able to select country/market code when they have chosen MMID as ID Type", () => {});
  // telephone number
  test("user should be able to enter their telephone number after select country/market when they have chosen MMID as ID Type", () => {});
  // MMID placeholder/sequence
  test("user should be able to enter MMID placeholder/sequence after enter telephone number when they have chosen MMID as ID Type", () => {});
  // display acc, mobile mmid
  test("user should see account, mobile number and MMID in ListHeader after enter MMID placeholder/sequence when they have chosen MMID as ID Type", () => {});
});

    `}</EuiCodeBlock>
  )
}
