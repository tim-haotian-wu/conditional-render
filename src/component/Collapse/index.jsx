import React from 'react'
import { PaymentContext } from 'container/CreateDemo/context'
import { Collapse } from 'antd'
import { SettingOutlined } from '@ant-design/icons'
import Card from 'component/Card'
const { Panel } = Collapse
const genExtra = () => (
  <SettingOutlined
    onClick={(event) => {
      event.stopPropagation()
    }}
  />
)

//   const addComponent = async (name) => {
//     console.log(`Loading ${name} component...`);
//     import(`./components/${name}/index.jsx`)
//       .then((component) => {
//         setMemberComponent(component.default);
//       })
//       .catch((error) => {
//         console.error(`"${name}" not yet supported`);
//       });
//   };

export default ({ handleChangeCollapeTitle, collapses }) => {
  return (
    <PaymentContext.Consumer>
      {({ dataFromApi }) => (
        <Collapse defaultActiveKey={['0']} onChange={() => {}}>
          {collapses.map((col, index) => {
            const { title, uuid, cards } = col
            return (
              <Panel header={title} key={index} extra={genExtra()}>
                {cards.map((card, j) => {
                  const { formGroups } = card
                  return (
                    <Card
                      key={j}
                      index={j}
                      {...{
                        index,
                        title,
                        uuid,
                        formGroups,
                        handleChangeCollapeTitle,
                      }}
                    />
                  )
                })}
              </Panel>
            )
          })}
        </Collapse>
      )}
    </PaymentContext.Consumer>
  )
}
