import React from 'react'
import { Button, Space } from 'antd'
import FormGroup from 'component/FormGroup'

export default ({
  index,
  formGroups,
  title,
  uuid,
  handleChangeCollapeTitle,
}) => {
  return (
    <Space direction="vertical">
      {formGroups.map((group, index) => {
        return <FormGroup {...group} />
      })}
      <Button
        onClick={() =>
          handleChangeCollapeTitle({
            uuid,
            title: `${title} ${index + 1}`,
          })
        }
      >
        change title for {index}
      </Button>
    </Space>
  )
}
